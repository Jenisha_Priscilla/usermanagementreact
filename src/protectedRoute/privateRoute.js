import React from "react";
import { Route } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import LoginModal from "../component/modal/loginModal";

const PrivateRoute = ({ component: Component, ...rest}) => { 

return(
  <Route {...rest} render = {(props) => (
    localStorage.getItem('token')
      ? <Component {...props}/>
      : <LoginModal state = {{ from: props.location}}/>
  )} />
)}

export default  withRouter((PrivateRoute))