import React from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/authAction'
import '../css/nav.css';

const Navg = (props) => {
    const style = {
      color : 'grey',
      padding : '0.5rem 1rem',
      textDecoration : 'none'
    }

    return (
      <Navbar  collapseOnSelect expand = "lg" variant = 'dark' >
        <Navbar.Toggle />
        <Navbar.Collapse >
          
        {
          localStorage.getItem('token') ?
          <>
            <Nav className = "mr-auto">
              <Link style = {style}  to = '/'> Home </Link>
              <Link to = '/users/new' style = {style}> Add user</Link>
            </Nav>
            <Nav>
              <Link to = '/'  onClick = {props.logout} style = {style}> Logout </Link>
            </Nav>
          </>
          : <Nav> <Link to = '/'  style = {style}> Login </Link> </Nav>
        }
        
        </Navbar.Collapse>
      </Navbar>  
    )
}

const mapStateToProps = state => {
  return {
      auth : state.auth.isLoggedIn
  }
}

const mapDispatchToProps = dispatch => {
  return {
      logout : () => dispatch(actionTypes.logout()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navg)

