import React, { Component } from "react";
import axios from 'axios';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card'
import Table from 'react-bootstrap/Table';
import EducationList from './userForm/educationList';
import MobileNumberList from './userForm/mobileNumberList';
import EmailAddressList from './userForm/emailAddressList';
import Button from 'react-bootstrap/Button';
import Spinner from '../spinner/spinner';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/userAction'
import {sessionExpired} from '../store/actions/authAction'

class updateUser extends Component
{
    constructor() {
        super();
        this.state = {
            disableMale : false,
            disableFemale : false,
            fieldError : '',
            emailError : '',
            mobileNumberError: '',
            education : [],
            mobileNumber :[],
            emailAddress : [],
            obtainedPercentageError : ''
        }
    }

    componentDidMount() {
        this.props.getUser(this.props.match.params.id)
        this.props.getBloodGroupList()
        this.props.update()
    }

    onCheck = (e) => {
        if(e.target.checked) {
            this.props.onCheckGender(e)
            if(e.target.value === "male") {
                this.setState({disableFemale : true})
            } else {
                this.setState({disableMale : true})
            }
        }    
    }

    validate () {
        var fieldError = " ";
        var emailError = " ";
        var mobileNumberError = " ";
        var obtainedPercentageError = " ";

        for(var i = 0; i < this.props.educationList.length; i++) {
            if(this.props.educationList[i]['course'] == '' || this.props.educationList[i]['domain'] == ''  ||
             this.props.educationList[i]['university'] == ''  || this.props.educationList[i]['obatinedPercentage'] == ''  ) {
                fieldError = '* All fields must be filled';
            }    
        } 
        for(var i = 0; i < this.props.mobileNumberList.length; i++) {
            if(this.props.mobileNumberList[i]['mobileNumber'] == '') {
                fieldError = '* All fields must be filled';
            }    
        }
        for(var i = 0; i < this.props.emailAddressList.length; i++) {
            if(this.props.emailAddressList[i]['emailAddress'] == '' ) {
                fieldError = '* All fields must be filled';
            }    
        } 
        if(this.props.firstName == '' || this.props.lastName == '' 
            || this.props.gender == '' || this.props.bloodGroup == '' || this.props.dateOfBirth == '') {
                fieldError = '* All fields must be filled';
        }

        if(fieldError == " ") { 
            for(var i = 0; i < this.props.emailAddressList.length; i++) {
                const emailValid = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!emailValid.test(this.props.emailAddressList[i]['emailAddress'])) {
                    emailError = 'Enter valid email address';
                }
            }
            var mobileNumberPattern = /^[6-9]\d{9}$/;
            for(var i = 0; i < this.props.mobileNumberList.length; i++) {
                if (!this.props.mobileNumberList[i]['mobileNumber'].match(mobileNumberPattern)) {
                    mobileNumberError = 'Enter valid mobile number';
                }
            }
            for(var i = 0; i < this.props.educationList.length; i++) {
                if(this.props.educationList[i]['obtainedPercentage'] <= 0 || this.props.educationList[i]['obtainedPercentage'] > 100 ) {
                    obtainedPercentageError = 'Percentage must be between 1 to 100';
                }
            }
        }

        if(fieldError != " " || emailError !=  " " || mobileNumberError !=  " " || obtainedPercentageError !=  " ")  {
            this.setState({ fieldError, emailError, obtainedPercentageError, mobileNumberError})
            return false;
        }
        return true;    
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if(isValid) {
            const obj = {
                firstName : this.props.firstName,
                lastName : this.props.lastName,
                gender : this.props.gender,
                dateOfBirth : this.props.dateOfBirth,
                bloodGroup : this.props.bloodGroup,
                mobileNumber : this.props.mobileNumberList,
                education : this.props.educationList,
                emailAddress : this.props.emailAddressList,
            }

            axios.post('api/users/edit/'+this.props.match.params.id, obj,
            {
                headers : {
                    'X-AUTH-TOKEN' : localStorage.getItem('token') 
                }
            })
            .then((response) => {
                alert(response.data.message);
                this.props.history.push('/users/'+this.props.match.params.id)
            })
            .catch((err) => {
                if(err.response.request.status === 400) {
                    let errors = Object.keys(err.response.data.error)
                    errors.forEach(function(object, i) {
                        this.setState({[object] : err.response.data.error[object]})     
                    }.bind(this))
                }
                if(err.response.request.status === 401)
                    this.props.sessionExpired()
            })    
        }       
    }

    render() {
        const style = {
            fontSize : 12,
            color :'red'
        }
        var today = new Date();
        var dd = today.getDate() ;
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if (dd<10) {
            dd='0'+dd;
        } 
      
        if (mm<10) {
            mm='0'+mm;
        } 
        today = yyyy+'-'+mm+'-'+dd;

        if(this.props.loading) {
            return <Spinner  />
        }
        
        return(
            <Row className = "justify-content-md-center">
                <Col xs lg = "9"> 
                    <Card bg = "light">
                        <Card.Body>
                            <Form onChange = {this.props.onChange}>
                                <div style = {style}>
                                    {this.state.fieldError}
                                </div>  
                                <div style = {style}>
                                    {this.state.firstName}
                                </div>  
                                <Form.Row>
                                    <Form.Label  column lg = {2}> First name </Form.Label>
                                        <Col sm = "5">
                                            <Form.Control type = "text" name = "firstName"
                                                placeholder = "First name" 
                                                value = {this.props.firstName}
                                            />
                                        </Col>
                                </Form.Row>
                                <br/>
                                <div style = {style}>
                                    {this.state.lastName}
                                </div>  
                                <Form.Row>
                                    <Form.Label  column lg = {2}> Last name </Form.Label>
                                        <Col sm = "5">
                                            <Form.Control type = "text" name = "lastName"
                                                placeholder = "Last name" 
                                                value = {this.props.lastName}
                                            />
                                        </Col>
                                </Form.Row>
                                <br/>
                                <Form.Row>
                                    <Form.Label  column lg = {2}> Date of birth </Form.Label>
                                        <Col sm = "5">
                                            <Form.Control type = "date" placeholder = "Data of birth" 
                                                name = "dateOfBirth"
                                                value = {this.props.dateOfBirth}
                                                max = {today} required
                                            />
                                        </Col>
                                </Form.Row>
                                <br/>
                                <Form.Row>
                                    <Form.Label  column lg = {2}> Gender </Form.Label>
                                    <Col lg = {1}>
                                    <Form.Check type = "radio" >
                                        <Form.Check.Input value = "male"  type = "radio" disabled = {this.state.disableMale}
                                        checked = {this.props.checkMale} style = {{ color: 'black' }} onClick = {this.onCheck}
                                        />
                                        <Form.Check.Label> Male </Form.Check.Label>
                                    </Form.Check>
                                    </Col>
                                    <Form.Check type = "radio" >
                                        <Form.Check.Input value = "female"  type = "radio" disabled = {this.state.disableFemale}
                                            checked = {this.props.checkFemale} style = {{ color: 'black' }} onClick = {this.onCheck}
                                        />
                                        <Form.Check.Label> Female </Form.Check.Label>
                                    </Form.Check>
                                </Form.Row>
                                <br/>
                                <Form.Row>
                                    <Form.Label  column lg = {2}> Blood group </Form.Label>
                                    <Col sm = "5">
                                        <Form.Control as = "select" name = "bloodGroup"  >
                                                <option disabled selected> {this.props.bloodGroup}</option>
                                                { this.props.bloodGroupList.map (function(object, i) {  
                                                    return  <option key = {i}> {object}  </option>; 
                                                })}
                                        </Form.Control>
                                    </Col>
                                </Form.Row>
                                <h5 style = {{textAlign :'center', margin: '20px'}}> Education details </h5>
                                <div style = {style}>
                                    {this.state.obtainedPercentageError}
                                </div>  
                                <Table responsive style = {{width:'800', marginTop : '10px'}}>
                                    <thead>
                                        <tr>
                                            <th> Course </th>
                                            <th> Domain </th>
                                            <th> University </th>
                                            <th> Obtained percentage </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <EducationList 
                                            add = {this.props.addNewEducationRow} 
                                            delete = {this.props.deleteEducationRow} 
                                            educationList = {this.props.educationList} 
                                            educationErrorList = {this.state.education}
                                        />
                                    </tbody>
                                </Table>    

                                <Row>
                                    <Col  xs = {4}>
                                            <div style = {style}>
                                                {this.state.mobileNumberError}
                                            </div> 
                                        <Table responsive style = {{marginTop : '10px'}}>
                                            <thead>
                                                <tr>
                                                    <th> Mobile number</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <MobileNumberList 
                                                    add = {this.props.addMobileNumberRow} 
                                                    delete = {this.props.deleteMobileNumberRow} 
                                                    mobileNumberList = {this.props.mobileNumberList} 
                                                    mobileNumberErrorList = {this.state.mobileNumber}
                                                />
                                            </tbody>
                                        </Table>  
                                    </Col>
                                    <Col>
                                        <div style = {style}>
                                            {this.state.emailError}
                                        </div>
                                        <Table responsive style = {{ marginTop : '10px'}}>
                                            <thead>
                                                <tr>
                                                    <th> Email address</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <EmailAddressList 
                                                    add = {this.props.addEmailAddressRow} 
                                                    delete = {this.props.deleteEmailAddressRow} 
                                                    emailAddressList = {this.props.emailAddressList} 
                                                    emailAddressErrorList = {this.state.emailAddress}
                                                />
                                            </tbody>
                                        </Table>  
                                    </Col>
                                </Row>
                                
                                <Button variant = "dark" style = {{margin: "auto", display:"block"}}
                                onClick = {this.handleSubmit}> Submit </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => {
    return {
        emailAddressList : state.user.emailAddressList,
        educationList : state.user.educationList,
        mobileNumberList : state.user.mobileNumberList,
        firstName : state.user.firstName,
        lastName : state.user.lastName,
        gender : state.user.gender,
        dateOfBirth : state.user.dateOfBirth,
        bloodGroup : state.user.bloodGroup,
        checkMale : state.user.checkMale,
        checkFemale : state.user.checkFemale,
        bloodGroupList : state.user.bloodGroupList,
        loading : state.user.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUser : (id) => dispatch(actionTypes.getUser(id)),
        onChange : (e) => dispatch(actionTypes.handleChange(e)),
        addNewEducationRow : () => dispatch(actionTypes.addNewEducationRow()),
        deleteEducationRow : (record) => dispatch(actionTypes.deleteEducationRow(record)),
        addEmailAddressRow: () => dispatch(actionTypes.addEmailAddressRow()),
        deleteEmailAddressRow : (record) => dispatch(actionTypes.deleteEmailAddressRow(record)),
        addMobileNumberRow: () => dispatch(actionTypes.addMobileNumberRow()),
        deleteMobileNumberRow : (record) => dispatch(actionTypes.deleteMobileNumberRow(record)),
        onCheckGender : (e) => dispatch(actionTypes.onCheckGender(e)),
        getBloodGroupList : () => dispatch(actionTypes.getBloodGroupList()),
        update : () =>  dispatch(actionTypes.update()),
        sessionExpired: () => dispatch(sessionExpired())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(updateUser)
