import React, { Component } from 'react'

class MobileNumberList extends Component
{
    render(){
        let result =  (
            <tr>
                <td>
                    {this.props.obj.mobileNumber}
                </td>
            </tr>
        ); 
        return result;
    }
    
}
export default MobileNumberList;