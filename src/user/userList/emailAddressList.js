import React, { Component } from 'react'

class EmailAddressList extends Component
{
    render(){
        let result =  (
            <tr>
                <td>
                    {this.props.obj.emailAddress}
                </td>
            </tr>
        ); 
        return result;
    }
    
}
export default EmailAddressList;