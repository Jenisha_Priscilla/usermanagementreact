import React, { Component } from 'react'

class EducationList extends Component
{
    render(){
        let result =  (
            <tr>
                <td>
                    {this.props.obj.course}
                </td>
                <td>
                    {this.props.obj.domain}
                </td>
                <td>
                    {this.props.obj.university}
                </td>
                <td>
                    {this.props.obj.obtainedPercentage}
                </td>  
            </tr>
        ); 
        return result;
    }
    
}
export default EducationList;
