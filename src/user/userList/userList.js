import React, { Component } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { faStreetView } from "@fortawesome/free-solid-svg-icons";
import { withRouter } from 'react-router-dom';

class UserList extends Component
{
   
    render(){
        var date = new Date(this.props.obj.dateOfBirth.date);
        var dd = date.getDate();
        var mm = date.getMonth()+1; 
        var yyyy = date.getFullYear();
        if (dd<10) {
            dd = '0'+dd;
        } 
      
        if (mm<10) {
            mm = '0'+mm;
        } 
        date = yyyy+'-'+mm+'-'+dd;
        let result =  (
            <tr>
                <td>
                    {this.props.obj.firstName}
                </td>
                <td>
                    {this.props.obj.lastName}
                </td>
                <td>
                    {date}
                </td>
                <td>
                    {this.props.obj.gender}
                </td>
                <td>
                    {this.props.obj.bloodGroup}
                </td>     
                <td>
                    <div onClick = {() => this.props.history.push('/users/'+ this.props.obj.id)}> <FontAwesomeIcon icon = {faStreetView} size = "sm" /> </div>
                </td>
                <td>
                    <div onClick = {() => this.props.history.push('/users/edit/'+ this.props.obj.id)}> <FontAwesomeIcon icon = {faPen} size = "sm" /> </div>
                </td>
            </tr>
        ); 
        return result;
    }
    
}
export default withRouter(UserList);
