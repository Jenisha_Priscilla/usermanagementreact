import React, {  useState, useEffect } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import UserList from './userList/userList';
import '../css/pagination.css'
import Pagination from "react-js-pagination";
import Spinner from '../spinner/spinner';
import { connect } from 'react-redux';
import {sessionExpired} from '../store/actions/authAction'

const ViewUsers = (props) => {

    const [userData, setUserData] = useState([])
    const [pageCount, setPageCount] = useState()
    const [currentPage, setCurrentPage] = useState(1)
    const [loading, setLoading] = useState(true);

    useEffect(() => {
       receivedData()
    }, [])

    const receivedData = () => {
        axios.get('api/users/'+currentPage,
        {
            headers : {
                'X-AUTH-TOKEN' : localStorage.getItem('token') 
            }
        })
        .then(response => {
            console.log(response)
            setPageCount(response.data.data.count);
            setUserData(response.data.data.user);
            setLoading(false);
        })
        .catch(err => {
            if(err.response.request.status === 401)
                props.sessionExpired()
        })
    }

    const userList = () => {
        return userData.map (function(object, i) {
            return <UserList obj = {object} key ={i} />;        
        });  
    }

    const handleClick = (e) => {
        setCurrentPage(e)
        receivedData()
    }

    if(loading) {
        return <Spinner  />
    }

    return (
        <>
           <Table responsive hover>
                <thead>
                    <tr>
                        <th> First name</th>
                        <th> Last name</th>
                        <th> Date of Birth</th>
                        <th> Gender </th>
                        <th> Blood group </th>
                        <th colSpan = "2"> Action</th>
                    </tr>
                </thead>
                <tbody>
                    {userList()} 
                </tbody>
            </Table>
            <Pagination
                activePage = {currentPage}
                pageRangeDisplayed = {5}
                itemsCountPerPage = {1}
                totalItemsCount = {pageCount}
                onChange = {handleClick}
                disabledClass = 'pagination--disabled'
            />
        </>
    );
}

const mapDispatchToProps = dispatch => {
    return {
        sessionExpired: () => dispatch(sessionExpired())
    }
}

export default connect(null, mapDispatchToProps)(ViewUsers)
