import React, {memo} from 'react';
import Form from 'react-bootstrap/Form'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux'; 

const EmailAddressList = (props) =>
{
  const style = {
    fontSize : 12,
    color :'red'
  }

  let emailAddressError = [];
  if(props.emailAddressErrorList) {
    props.emailAddressErrorList.map((val, idx) => {
      emailAddressError[idx] = props.emailAddressErrorList[idx].emailAddress
    })
  }

    return (
      props.emailAddressList.map((val, idx) => {
        return (
          <>
            <tr>
              <td style = {{width :250}}> 
                <Form.Control 
                  type = "text"
                  placeholder = "Email Address" name = "emailAddress" data-id = {idx} 
                  value = {props.emailAddressList[idx].emailAddress} 
                />
                <div style = {style}>
                  {emailAddressError[idx]}
                </div>
              </td>
              <td>
                { 
                idx===0? <Button variant = "dark"  disabled = {props.isUpdate} onClick = {props.add} > <FontAwesomeIcon icon = {faPlus} size = "sm" /> </Button> : 
                         <Button variant = "danger" disabled = {props.isUpdate} onClick={(() => props.delete(val))} > <FontAwesomeIcon icon = {faMinus} size = "sm" /> </Button>
                }
              </td>
            </tr >
          </>
        )
      })
    )
}
const mapStateToProps = state => {
  return {
      isUpdate : state.user.update,
  }
}
  
export default connect(mapStateToProps, null)(EmailAddressList)
