import React, {memo} from 'react';
import Form from 'react-bootstrap/Form'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';

const MobileNumberList = (props) =>
{
  const style = {
    fontSize : 12,
    color :'red'
  }

  let mobileNumberError = [];
  if(props.mobileNumberErrorList){
    props.mobileNumberErrorList.map((val, idx) => {
      mobileNumberError [idx] = props.mobileNumberErrorList[idx].mobileNumber
    })
  }
 
    return (
      props.mobileNumberList.map((val, idx) => {
        return (
          <>
            <tr>
              <td style = {{width :250}}> 
                <Form.Control 
                  type = "text"
                  placeholder = "Mobile number" name = "mobileNumber" data-id={idx}  
                  value = {props.mobileNumberList[idx].mobileNumber} 
                />
                <div style = {style}>
                  {mobileNumberError[idx]}
                </div>
              </td>
              <td>
                { 
                idx===0? <Button variant = "dark" disabled = {props.isUpdate}  onClick = {props.add} > <FontAwesomeIcon icon = {faPlus} size = "sm" /> </Button> : 
                         <Button variant = "danger" disabled = {props.isUpdate} onClick={(() => props.delete(val))} > <FontAwesomeIcon icon = {faMinus} size = "sm" /> </Button>
                }
              </td>
            </tr >
          </>
        )
      })
    )
}
  
const mapStateToProps = state => {
  return {
      isUpdate : state.user.update,
  }
}

export default connect(mapStateToProps, null)(MobileNumberList)
