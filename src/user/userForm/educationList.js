import React, {memo} from 'react';
import Form from 'react-bootstrap/Form'
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';

const EducationList = (props) =>
{
  const style = {
    fontSize : 12,
    color :'red'
  }

  let courseError = [];
  let domainError = [];
  let universityError = [];
  let obtainedPercentageError = [];

  
  if(props.educationErrorList){
    props.educationErrorList.map((val, idx) => {
      courseError[idx] = props.educationErrorList[idx].course
      domainError[idx] = props.educationErrorList[idx].domain
      universityError[idx] = props.educationErrorList[idx].university
      obtainedPercentageError[idx] = props.educationErrorList[idx].obtainedPercentage
    })
  }
  

    return (
      props.educationList.map((val, idx) => {
        return (
          <>
            <tr>
              <td > 
                <Form.Control 
                  type = "text"
                  placeholder = "Course" name = "course" data-id={idx}  
                  value = {props.educationList[idx].course}
                />  
                <div style = {style}>
                  {courseError[idx]}
                </div>
              </td>

              <td>
                <Form.Control 
                  type = "text" placeholder = "Domain" name = "domain" 
                  value = {props.educationList[idx].domain}
                  data-id={idx}/>
                <div style = {style}>
                  {domainError[idx]}
                </div>
              </td>
              <td >
                <Form.Control 
                  type = "text" placeholder = "University" 
                  name = "university" data-id = {idx} 
                  value = {props.educationList[idx].university}/>
                <div style = {style}>
                  {universityError[idx]}
                </div>
              </td>
              <td style = {{width :200}}>
                <Form.Control 
                  type = "number" placeholder = "Obtained percent" 
                  name = "obtainedPercentage" data-id = {idx} 
                  value = {props.educationList[idx].obtainedPercentage}/>
                  <div style = {style}>
                    {obtainedPercentageError[idx]}
                  </div>
              </td>
              <td>
                { 
                idx===0? <Button variant = "dark" disabled = {props.isUpdate} onClick = {props.add} > <FontAwesomeIcon icon = {faPlus} size = "sm" /> </Button> : 
                         <Button variant = "danger" disabled = {props.isUpdate} onClick={(() => props.delete(val))} > <FontAwesomeIcon icon = {faMinus} size = "sm" /> </Button>
                }
              </td>
            </tr >
          </>
        )
      })
    )
}
const mapStateToProps = state => {
  return {
      isUpdate : state.user.update,
  }
}

export default connect(mapStateToProps, null)(EducationList)
