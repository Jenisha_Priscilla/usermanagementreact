import React, { useEffect } from "react";
import Col from 'react-bootstrap/Col'
import Table from 'react-bootstrap/Table';
import Row from 'react-bootstrap/Row'
import EducationList from './userList/educationList';
import MobileNumberList from './userList/mobileNumberList';
import EmailAddressList from './userList/emailAddressList';
import Spinner from '../spinner/spinner';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/userAction'

const ViewOneUser = (props) => {

    useEffect(() => {
        props.getUser(props.match.params.id)
     }, [])

    const educationList = () => {
        return props.educationList.map (function(object, i) {
            return <EducationList obj = {object} key ={i} />;        
        });
    }

    const mobileNumberList = () => {
        return props.mobileNumberList.map (function(object, i) {
            return <MobileNumberList obj = {object} key ={i} />;        
        });
    }

    const emailAddressList = () => {
        return props.emailAddressList.map (function(object, i) {
            return <EmailAddressList obj = {object} key ={i} />;        
        });
    }

    const style = {
        margin : '10px'
    }
        
    if(props.loading) {
        return <Spinner  />
    }

    return (
        <>
            <Row style = {style}>
                <Col xs = {3}> First name</Col>
                <Col>{props.firstName}</Col>
            </Row>
            <Row style = {style}>
                <Col xs = {3}> Last name</Col>
                <Col>{props.lastName}</Col>
            </Row>
            <Row style = {style}>
                <Col xs = {3}> Gender </Col>
                <Col>{props.gender}</Col>
            </Row>
            <Row style = {style}>
                <Col xs = {3}> Date of birth </Col>
                <Col>{props.dateOfBirth}</Col>
            </Row>
            <Row style = {style}>
                <Col xs = {3}> Blood group </Col>
                <Col>{props.bloodGroup}</Col>
            </Row>
            <h5 style = {{textAlign :'center', margin: '20px'}}> Education details </h5>
            <Table responsive hover style = {{justifyContent : 'center', width : '600'}}>
                <thead>
                    <tr>
                        <th> Course </th>
                        <th> Domain </th>
                        <th> University </th>
                        <th> Obtained percentage </th>
                    </tr>
                </thead>
                <tbody>
                    {educationList()} 
                </tbody>
            </Table>
            <Row>
                <Col  xs = {4}>
                    <Table responsive hover>
                        <thead>
                            <tr>
                                <th> Mobile number </th>
                            </tr>
                        </thead>
                        <tbody>
                            {mobileNumberList()} 
                        </tbody>
                    </Table>
                </Col>
                <Col  xs = {5}>
                    <Table responsive hover>
                        <thead>
                            <tr>
                                <th> Email Address </th>
                            </tr>
                        </thead>
                        <tbody>
                            {emailAddressList()} 
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </>
    )
    
}

const mapStateToProps = state => {
    return {
        emailAddressList : state.user.emailAddressList,
        educationList : state.user.educationList,
        mobileNumberList : state.user.mobileNumberList,
        firstName : state.user.firstName,
        lastName : state.user.lastName,
        gender : state.user.gender,
        dateOfBirth : state.user.dateOfBirth,
        bloodGroup : state.user.bloodGroup,
        loading : state.user.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUser : (id) => dispatch(actionTypes.getUser(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewOneUser)
