import React from 'react'
import Col from 'react-bootstrap/Col'
 const Layout = (props) => {
    const mystyle = {
        minHeight : '100vh',
        marginTop : '40px',
        marginBottom : '40px'
      };
      
    return (
        <React.Fragment >
            {props.nav}
            <div style = {mystyle}>
                <Col> {props.children} </Col> 
            </div>     
        </React.Fragment>
    )
 }
 export default Layout;