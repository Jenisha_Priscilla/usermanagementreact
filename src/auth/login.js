import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import axios from 'axios'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions/authAction'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            emailAddress: '',
            password: '',
            inValidCredentials: '',
            fieldError: '',
            emailError: ''
        }
    }
    handleChange = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }

    validate = () => {
        var fieldError = "";
        var emailError = "";
        const emailValid = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.state.email && this.state.password) {
            fieldError = '* All fields must be filled';
        }
        if (!emailValid.test(this.state.emailAddress)) {
            emailError = 'Enter valid email address';
        }

        if(fieldError != "" || emailError !=  "" )  {
            this.setState({ fieldError, emailError })
            return false;
        }
        return true;   
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const isValid = this.validate()
        if (isValid) {
            this.setState({ fieldError: "", emailError: ""})
            const obj = {
                emailAddress:this.state.emailAddress,
                password:this.state.password
            };
            axios.post('/login', obj)
            .then(response => {
                localStorage.setItem('token', response.data.token);
                this.props.authSuccess()
            })
            .catch(err => this.setState({inValidCredentials :err.response.data.message}))
        } 
       
    }

    render() {
        let login = (
            <Row style = {{justifyContent: "center"}}>
                <Col md = {7}> 
                    <Form onChange = {this.handleChange}>
                        <div style ={{ fontSize:12, color:"red"}}>
                            {this.state.fieldError}
                        </div> 
                        <Form.Row>
                            <Form.Label column lg = {2}> Email address </Form.Label>
                            <Col md = "5">
                                <Form.Control type = "email" placeholder = "Enter email" name = "emailAddress"/>
                            </Col>
                        </Form.Row>
                        <div style = {{fontSize:12, color:"red"}}>
                            {this.state.emailError}
                        </div> 
                        <br/>
                        <Form.Row>
                            <Form.Label column lg = {2}> Password </Form.Label>
                            <Col md = "5">
                                <Form.Control type = "password" placeholder = "Password" name = "password" />
                            </Col>
                        </Form.Row>
                        <div style = {{fontSize:12, color:"red"}}>
                            {this.state.inValidCredentials}
                        </div> 
                        <br/>
                        <Button variant = "dark" style = {{ display:"block"}} onClick = {this.handleSubmit}>
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row >     
        )
        if (localStorage.getItem('token')) { 
            login = <Redirect to =  '/' />
        } 
       return login;
    }
}

const mapStateToProps = state => {
    return {
        auth : state.auth.isLoggedIn
    }
}
const mapDispatchToProps = dispatch => {
    return {
        authSuccess : () => dispatch(actionTypes.authSuccess())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

