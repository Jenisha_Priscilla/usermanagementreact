import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import axios from 'axios';
import  {Provider} from 'react-redux'
import { createStore, applyMiddleware, compose ,combineReducers} from 'redux'
import userReducer from './store/reducers/userReducer'
import thunk from 'redux-thunk'
import reportWebVitals from './reportWebVitals';
import authReducer from './store/reducers/authReducer';

axios.defaults.baseURL = 'https://127.0.0.1:8000'
axios.defaults.headers.common['X-AUTH-TOKEN'] = localStorage.getItem('token') ;

const rootReducer = combineReducers ({
  user : userReducer,
  auth : authReducer
}); 

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));
const app = (
  <Provider store = {store}>
      <App/>
  </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
