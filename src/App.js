import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import React, { Component } from 'react';
import ViewUsers from './user/viewUsers'
import AddUser from './user/addUser';
import Login from './auth/login'
import UpdateUser from './user/updateUser';
import ViewOneUser from './user/viewOneUser';
import Navg from './component/nav';
import Layout from './layout/layout';
import { connect } from 'react-redux';

class App extends Component 
{
  render()
  {
    let navBar = <Navg/>
    let route = (
      <Switch>
        <Route path = '/' component = {Login}/>
      </Switch>
    )

    if(localStorage.getItem('token') || this.props.isLoggedIn){
    route = (
        <>
          <Switch>
            <Route path = '/users/new' component = {AddUser} />  
            <Route path = '/users/edit/:id' component = {UpdateUser}/>
            <Route path = '/users/:id' component = {ViewOneUser}/>
            <Route path = '/login' component = {Login}/>
            <Route path = '/' component = {ViewUsers} />  
          </Switch>
        </>
     );
    }
    
   let app = (
		<Router>
			<Layout nav = {navBar}>			 
					{route}
			</Layout>
		</Router>
  	)
   return app;
  }
}

const mapStateToProps = state => {
  return {
      auth : state.auth.isLoggedIn
  }
}
export default connect(mapStateToProps, null)(App)
