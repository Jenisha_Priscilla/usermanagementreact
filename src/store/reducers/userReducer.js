import { ADD_EMAIL_ADDRESS_ROW, ADD_MOBILE_NUMBER_ROW, ADD_NEW_EDUCATION_ROW, ADD_USER, DELETE_EDUCATION_ROW, DELETE_EMAIL_ADDRESS_ROW, DELETE_MOBILE_NUMBER_ROW, HANDLE_CHANGE, ON_CHECK_GENDER, SET_BLOOD_GROUP, SET_USER, UPDATE_USER } from "../actions/actionTypes";

const initialState = {
    educationList : [{ course: '', domain: '', university: '', obtainedPercentage: ''}],
    mobileNumberList :[{mobileNumber: ''}],
    emailAddressList :[{emailAddress: ''}],
    firstName: '',
    lastName :'',
    gender:'',
    bloodGroup: '',
    dateOfBirth: '',
    checkFemale :false,
    checkMale : false,
    bloodGroupList: [],
    loading : true,
    update : false
};

const userReducer = (state = initialState, action) => {
    switch(action.type) {
        
        case HANDLE_CHANGE:
            const updatedEducationList = [...state.educationList]
            const updatedMobileNumberList = [...state.mobileNumberList]
            const updatedEmailAddressList = [...state.emailAddressList]
            var updatedFirstName = state.firstName
            var updatedLastName = state.lastName
            var updatedGender = state.gender
            var updatedBloodGroup = state.bloodGroup
            var updatedDateOfBirth = state.dateOfBirth
            if (["course", "domain", "university", "obtainedPercentage"].includes(action.event.target.name)) {  
                updatedEducationList[action.event.target.dataset.id][action.event.target.name] = action.event.target.value;
            } 
            if (["mobileNumber"].includes(action.event.target.name)) {      
                updatedMobileNumberList[action.event.target.dataset.id][action.event.target.name] = action.event.target.value;
            } 
            if (["emailAddress"].includes(action.event.target.name)) {    
                updatedEmailAddressList[action.event.target.dataset.id][action.event.target.name] = action.event.target.value;
            } 
            if (["firstName"].includes(action.event.target.name)) {    
                updatedFirstName = action.event.target.value
            }
            if (["lastName"].includes(action.event.target.name)) {    
                updatedLastName = action.event.target.value
            }
            if (["gender"].includes(action.event.target.name)) {    
                updatedGender = action.event.target.value
            }
            if (["bloodGroup"].includes(action.event.target.name)) {    
                updatedBloodGroup = action.event.target.value
            }
            if (["dateOfBirth"].includes(action.event.target.name)) {    
                updatedDateOfBirth = action.event.target.value
            }

            return {
                ...state,
                educationList: updatedEducationList,
                mobileNumberList: updatedMobileNumberList,
                emailAddressList: updatedEmailAddressList,
                firstName : updatedFirstName,
                lastName : updatedLastName,
                gender : updatedGender,
                bloodGroup : updatedBloodGroup,
                dateOfBirth : updatedDateOfBirth
            }

        case ADD_NEW_EDUCATION_ROW:
            return {
                ...state,
                educationList: [...state.educationList,{ course: '', domain: '', university: '', obtainedPercentage: ''}],
            }

        case DELETE_EDUCATION_ROW:
            return {
                ...state,
                educationList: state.educationList.filter(r => r !== action.record)
            }

        case ADD_EMAIL_ADDRESS_ROW:
            return {
                ...state,
                emailAddressList: [...state.emailAddressList, {emailAddress: ''}]
            }
            
        case DELETE_EMAIL_ADDRESS_ROW:
            return {
                ...state,
                emailAddressList: state.emailAddressList.filter(r => r !== action.record)
            }
        case ADD_MOBILE_NUMBER_ROW:
            return {
                ...state,
                mobileNumberList: [...state.mobileNumberList, {mobileNumber: ''}]
            }
            
        case DELETE_MOBILE_NUMBER_ROW:
            return {
                ...state,
                mobileNumberList: state.mobileNumberList.filter(r => r !== action.record)
            }

        case ON_CHECK_GENDER:
            var updatedCheckFemale = state.checkFemale
            var updatedCheckFemale = state.checkMale

            if(action.event.target.value === "female") {
                updatedCheckFemale = true
            } else {
                updatedCheckMale = true
            }
            return {
                ...state,
                gender : action.event.target.value,
                checkMale : updatedCheckMale,
                checkFemale : updatedCheckFemale
            }
        
        case SET_USER:
            var date = new Date(action.user.dateOfBirth.date);
            var dd = date.getDate();
            var mm = date.getMonth()+1; 
            var yyyy = date.getFullYear();
            if (dd<10) {
                dd = '0'+dd;
            } 
          
            if (mm<10) {
                mm = '0'+mm;
            } 
            date = yyyy+'-'+mm+'-'+dd;

            var updatedCheckMale = state.checkMale
            var updatedCheckFemale = state.checkFemale
            if(action.user.gender === "female") {
                updatedCheckFemale = true
            } else {
                updatedCheckMale = true
            }

            return {
                ...state,
                firstName : action.user.firstName,
                lastName : action.user.lastName,
                dateOfBirth : date,
                mobileNumberList : action.user.mobileNumber,
                educationList : action.user.education,
                emailAddressList : action.user.emailAddress,
                bloodGroup : action.user.bloodGroup,
                gender : action.user.gender,
                checkMale : updatedCheckMale,
                checkFemale : updatedCheckFemale,
                loading : false,
            }
        case SET_BLOOD_GROUP:
            return {
                ...state,
                bloodGroupList: action.bloodGroupList,
            }

        case ADD_USER:
            return {
                ...state,
                update : false,
                educationList : [{ course: '', domain: '', university: '', obtainedPercentage: ''}],
                mobileNumberList :[{mobileNumber: ''}],
                emailAddressList :[{emailAddress: ''}],
            }

        case UPDATE_USER:
            return {
                ...state,
                update : true,
            }

        default : 
            return state;
    }
};
export default userReducer;