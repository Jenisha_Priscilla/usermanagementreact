import { AUTH_SUCCESS, LOGOUT, SESSION_EXPIRED } from "../actions/actionTypes";

const initialState = {
    isLoggedIn : false
};

const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case AUTH_SUCCESS:
            return {
                ...state,
                isLoggedIn: true
            }

        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false
            }
        case SESSION_EXPIRED:
            return {
                ...state,
                isLoggedIn: false
            }

        default : 
            return state;
    }
};
export default authReducer;