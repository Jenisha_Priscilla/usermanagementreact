import { ADD_EMAIL_ADDRESS_ROW, ADD_MOBILE_NUMBER_ROW, ADD_NEW_EDUCATION_ROW, ADD_USER, DELETE_EDUCATION_ROW, DELETE_EMAIL_ADDRESS_ROW, DELETE_MOBILE_NUMBER_ROW, HANDLE_CHANGE, ON_CHECK_GENDER, SET_BLOOD_GROUP, SET_USER, UPDATE_USER } from "./actionTypes";
import axios from 'axios';
import { sessionExpired } from "./authAction";

export const handleChange = (event) => {
    return {
        type: HANDLE_CHANGE,
        event : event
    }
};

export const addNewEducationRow = () => {
    return {
        type: ADD_NEW_EDUCATION_ROW
    }
};

export const deleteEducationRow = (record) => {
    return {
        type: DELETE_EDUCATION_ROW,
        record : record
    }
};

export const addEmailAddressRow = () => {
    return {
        type: ADD_EMAIL_ADDRESS_ROW
    }
};

export const deleteEmailAddressRow = (record) => {
    return {
        type: DELETE_EMAIL_ADDRESS_ROW,
        record : record
    }
};
export const addMobileNumberRow = () => {
    return {
        type: ADD_MOBILE_NUMBER_ROW
    }
};

export const deleteMobileNumberRow = (record) => {
    return {
        type: DELETE_MOBILE_NUMBER_ROW,
        record : record
    }
};

export const onCheckGender = (event) => {
    return {
        type: ON_CHECK_GENDER,
        event : event
    }
};


export const getUser = (id) => {
    return dispatch => {
        axios.get('/api/user/'+id,
        {
            headers : {
                'X-AUTH-TOKEN' : localStorage.getItem('token') 
            }
        })
        .then(response => {
            dispatch(setUser(response.data.data.user))
        })
        .catch(err =>  {
            if(err.response.request.status === 401)
                dispatch(sessionExpired())
        })
    }
};

export const setUser = (user) => {
    return {
        type: SET_USER,
        user : user
    }
};

export const getBloodGroupList = () => {
    return dispatch => {
        axios.get('/bloodGroup')
        .then(response => {
            console.log(response.data)
            dispatch(setBloodGroupList(response.data.data.bloodGroup))
        })
        .catch(err => console.log(err))
    }
};

export const setBloodGroupList = (bloodGroupList) => {
    console.log(bloodGroupList)
    return {
        type: SET_BLOOD_GROUP,
        bloodGroupList : bloodGroupList
    }
};

export const add = () => {
    return {
        type: ADD_USER,
    }
};

export const update = () => {
    return {
        type : UPDATE_USER,
    }
}



