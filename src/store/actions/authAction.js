import { AUTH_SUCCESS, LOGOUT, SESSION_EXPIRED } from "./actionTypes";

export const authSuccess = () => {
    return {
        type: AUTH_SUCCESS
    }
};

export const logout = () => {
    localStorage.clear()
    return {
        type: LOGOUT
    }
};

export const sessionExpired = () => {
    alert('Token expired')
    localStorage.clear()
    return {
        type: SESSION_EXPIRED,
    }
}