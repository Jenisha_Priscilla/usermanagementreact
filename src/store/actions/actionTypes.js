export const HANDLE_CHANGE = 'HANDLE_CHANGE';

export const ADD_NEW_EDUCATION_ROW = 'ADD_NEW_EDUCATION_ROW';
export const DELETE_EDUCATION_ROW = 'DELETE_EDUCATION_ROW';

export const ADD_EMAIL_ADDRESS_ROW = 'ADD_EMAIL_ADDRESS_ROW';
export const DELETE_EMAIL_ADDRESS_ROW = 'DELETE_EMAIL_ADDRESS_ROW';

export const ADD_MOBILE_NUMBER_ROW = 'ADD_MOBILE_NUMBER_ROW';
export const DELETE_MOBILE_NUMBER_ROW = 'DELETE_MOBILE_NUMBER_ROW';

export const ON_CHECK_GENDER = 'ON_CHECK_GENDER';

export const SET_USER = 'SET_USER';
export const SET_BLOOD_GROUP = 'SET_BLOOD_GROUP';

export const ADD_USER = 'ADD_USER';
export const UPDATE_USER = 'UPDATE_USER';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const LOGOUT = 'LOGOUT';

export const SESSION_EXPIRED = 'SESSION_EXPIRED';